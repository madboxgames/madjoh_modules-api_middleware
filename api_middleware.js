define([
	'require',
	'madjoh_modules/msgbox/msgbox'
], 
function (require, MsgBox){
	var APIMiddleware = {
		handle : function(result, request, errors){
			if(result.status === 0 && result.errors.length > 0 && result.errors[0].code === 408) return; // FORM ERROR ERROR
			if(result.status === 1) 	return request.promiseActions.resolve(result);
			if(!request.options.sync) 	return request.promiseActions.reject(result);
			if(result.errors.length === 0){
				MsgBox.show('error');
				return request.promiseActions.reject(result);
			}
			
			var originCode = result.errors[0].code;

			console.log(result);

			var treated = errors.onError(originCode, result);
			if(!treated) MsgBox.show('error');
			
			return request.promiseActions.reject(result);
		}
	};

	return APIMiddleware;
});